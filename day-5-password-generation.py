#Password Generator Project
import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))

#Eazy Level - Order not randomised:
#e.g. 4 letter, 2 symbol, 2 number = JduE&!91


#Hard Level - Order of characters randomised:
#e.g. 4 letter, 2 symbol, 2 number = g^2jk8&P
password_l = ""
rand_letters = random.choices(letters,k = nr_letters)
for letter in rand_letters:
  password_l += letter
# print(password_l)

password_s = ""
rand_symbols = random.choices(symbols,k = nr_symbols)
for symbol in rand_symbols:
  password_s += symbol
# print(password_s)

password_n = ""
rand_numbers = random.choices(numbers,k = nr_numbers)
for number in rand_numbers:
  password_n += number
# print(password_n)
password = password_l + password_n + password_s

password=''.join(random.sample(password,len(password)))
print("Your password is "+ password)