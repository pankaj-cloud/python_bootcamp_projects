"""
capitals: {
    "France": "Paris",
    "Germany": "Berlin"
}

travel_logs: {
    "France": {"cities_visited":["Paris", "Dijon","Lille" ], "total_visits": 10} ,
    "Germany": {"cities_visited":["Berlin", "Hamburg","Stautgard" ], "total_visits":5 }
    
}
# Nesting a dictionary inside a list
travel_logs: [
    {
        "country": "France", 
        "cities_visited":["Paris", "Dijon","Lille" ], 
        "total_visits": 10
    } ,
    {
        "country":"Germany",
        "cities_visited":["Berlin", "Hamburg","Stautgard" ],
        "total_visits":5
    }
    
]
"""

country = input() # Add country name
visits = int(input()) # Number of visits
list_of_cities = eval(input()) # create list from formatted string

travel_log = [
  {
    "country": "France",
    "visits": 12,
    "cities": ["Paris", "Lille", "Dijon"]
  },
  {
    "country": "Germany",
    "visits": 5,
    "cities": ["Berlin", "Hamburg", "Stuttgart"]
  },
]
# Do NOT change the code above 👆

# TODO: Write the function that will allow new countries
# to be added to the travel_log. 
def add_new_country(country_visited, visits_done, list_of_cities_done):
    new_country : {}
    new_country[country_visited]= country
    new_country[visits_done]= visits
    new_country[list_of_cities_done]=list_of_cities
    travel_log.append(new_country)
# Do not change the code below 👇
add_new_country(country, visits, list_of_cities)
print(f"I've been to {travel_log[2]['country']} {travel_log[2]['visits']} times.")
print(f"My favourite city was {travel_log[2]['cities'][0]}.")